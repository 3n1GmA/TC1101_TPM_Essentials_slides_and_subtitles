1
00:00:01,640 --> 00:00:07,840
Welcome to this OST2 course. We're going to talk 
about when to integrate a trusted platform module  

2
00:00:07,840 --> 00:00:16,200
in your systems. Because there are so many use 
cases for a TPM nowadays, and I have personally  

3
00:00:16,200 --> 00:00:22,960
experienced a trend in using TPM on embedded 
systems, would that be a medical device, would that  

4
00:00:22,960 --> 00:00:31,920
be Internet of Things device, or even aerospace, 
it is easier and better to name when it is not  

5
00:00:31,920 --> 00:00:39,920
preferable to use a TPM. There are only three major 
situation where this could occur. Therefore in all  

6
00:00:39,920 --> 00:00:47,320
other scenarios I would advise that the TPM 
can be a good fit for hardware security in your  

7
00:00:47,320 --> 00:00:53,200
product. So let's start. The TPM does not offer 
just cryptographic functions and secure storage  

8
00:00:53,200 --> 00:00:59,040
but also protection against physical tampering, 
protection against side channel attacks, and so  

9
00:00:59,040 --> 00:01:06,120
much more. Therefore by design the TPM made 
a trade-off security over speed. While this is  

10
00:01:06,120 --> 00:01:12,520
true, and some people know it, often comes up the 
question "Okay but give us a sense of what is the  

11
00:01:12,520 --> 00:01:18,080
performance like?" and this is a fair question. The 
best resource I have found in my career is the  

12
00:01:18,080 --> 00:01:24,840
benchmarking done with the wolfTPM software stack. 
We've mentioned this stack in earlier lectures.  

13
00:01:24,840 --> 00:01:31,600
It's one of several well-known open-source TPM 
software libraries. Here I've taken the public  

14
00:01:31,600 --> 00:01:38,560
data that's published on the wolfTPM GitHub repo, 
and I have created a brief table. In green we can  

15
00:01:38,560 --> 00:01:47,120
see the best performance among the listed TPMs. 
I have selected the top two in each category. Also  

16
00:01:47,120 --> 00:01:54,280
important is to note when these models came to 
the market. As you could see for example at the  

17
00:01:54,280 --> 00:02:03,840
beginning of the table we have Infineon SLB9670 
and the new model 9672. The old model came about  

18
00:02:03,840 --> 00:02:15,080
2015-2016 the new model came I think end of 
2021 and was mass available last year so 2022.  

19
00:02:15,080 --> 00:02:21,360
And we can see the improvements. And this is also 
true for the last entry in the table the Nations  

20
00:02:21,360 --> 00:02:28,320
Technology Z32 TPM which is a Chinese manufacturer 
so it is not widely available. Typically this is  

21
00:02:28,320 --> 00:02:36,560
used only in China. And it is if I'm not 
mistaken it was released in around 2019. Therefore  

22
00:02:36,560 --> 00:02:42,480
it also benefited from some advancements and as 
you could see it offers a good performance. So in  

23
00:02:42,480 --> 00:02:52,680
our table the latest from Infineon SLB9672 
and the more recent Nations Tech Z32 outperform the  

24
00:02:52,680 --> 00:02:59,360
rest in my opinion because they were released 
last. So they benefit most from the technology  

25
00:02:59,360 --> 00:03:06,160
advancement. However, there are common metrics 
here I'll need to highlight how much time it  

26
00:03:06,160 --> 00:03:12,880
takes to generate a primary key with RSA. And this 
is indeed a significant amount of time. While this  

27
00:03:12,880 --> 00:03:19,480
might discourage at first some of you to consider 
using a TPM you need to know that this operation  

28
00:03:19,480 --> 00:03:25,960
does not happen often. And even if it does, we 
have a way to persist the key so we don't have  

29
00:03:25,960 --> 00:03:31,680
to recreate it every time between operations. And 
also once the key is created it's easier to load  

30
00:03:31,680 --> 00:03:37,720
it especially if let's say we we want to work 
more with the public part of the key pair and so on  

31
00:03:37,720 --> 00:03:44,160
and so on. So these operations are indeed time 
expensive compared to what we are used nowadays  

32
00:03:44,160 --> 00:03:51,760
with CPUs in our laptops and maybe some more 
powerful smartphones. At the same time here we get  

33
00:03:51,760 --> 00:03:57,160
more security and not so much speed and this is 
intentional. And that's a good thing, because we're  

34
00:03:57,160 --> 00:04:04,120
talking about security here. Now if we look at 
the ECC keys, asymmetric keys, the picture changes  

35
00:04:04,120 --> 00:04:11,920
radically. Because ECC it's so much more modern 
it's even embedded friendly. And as you probably  

36
00:04:11,920 --> 00:04:17,040
know it's a very preferred choice for the Internet 
of Things and small devices, memory constrained  

37
00:04:17,040 --> 00:04:23,000
devices. And here we get a more performance out of 
the TPM, although it is not designed for that. So  

38
00:04:23,000 --> 00:04:29,840
using the TPM with ECC for devices in the field 
is great in my opinion, it offers us high great  

39
00:04:29,840 --> 00:04:36,400
security at a very good performance level. Because 
again, these operations do not happen that often  

40
00:04:36,400 --> 00:04:42,880
here we have the ECDSA sign and verify operations 
for digital signatures and the scheme just  

41
00:04:42,880 --> 00:04:51,000
to mention it is for ECC is the curve for ECC is 
P256. Now you can also do hash operations and you  

42
00:04:51,000 --> 00:04:57,160
can use the TPM as a random number generator 
as a source of randomness of true randomness.  

43
00:04:57,160 --> 00:05:03,200
Of course this comes at a specific bandwidth so this 
needs to be taken into account depending on your  

44
00:05:03,200 --> 00:05:08,960
application. It is just that if you're thinking of 
using the TPM as a cryptographic accelerator just  

45
00:05:08,960 --> 00:05:13,240
looking at this numbers you already know this 
is not a good idea this is not a good fit for  

46
00:05:13,240 --> 00:05:19,600
your case. And that's okay because TPM is meant for 
security not speed, this will be a recurring theme  

47
00:05:19,600 --> 00:05:24,440
in the lectures. Because it is important 
and people are used to getting some sort of  

48
00:05:24,440 --> 00:05:29,920
cryptographic accelerators along with the security 
features. Here we really focus on the security  

49
00:05:29,920 --> 00:05:37,360
matter. Another typical scenario is that once we 
start using the TPM we see its benefits. And then  

50
00:05:37,360 --> 00:05:45,600
we try to use it for everything. Of course this is 
extremely difficult because of the limitations in  

51
00:05:45,600 --> 00:05:51,920
memory size. And again this comes down to cost and 
this comes down to the fact that the TPM building  

52
00:05:51,920 --> 00:05:58,520
fabbing out the TPM chip, the physical chip, costs 
money the more memory it has. So the TPM has  

53
00:05:58,520 --> 00:06:05,720
enough memory for critical data for, would 
it be certificates, would it be keys, would it be  

54
00:06:05,720 --> 00:06:12,520
some other form of secrets, or configuration 
system device user, up to your requirements.  

55
00:06:12,520 --> 00:06:19,720
It is a mistake, and I've seen that, to try to fit 
everything inside the TPM non-volatile memory  

56
00:06:19,720 --> 00:06:25,280
because it simply cannot fit there. Now there 
is a trend of including more and more memory  

57
00:06:25,280 --> 00:06:33,760
because of customer demand. And on different needs as 
The Internet of Things landscape grows. And we've  

58
00:06:33,760 --> 00:06:40,840
seen this. The old Infineon TPM model had only 
7 kilobytes. And of course there were variants  

59
00:06:40,840 --> 00:06:48,200
later that went up I think to almost 24 at one 
point depending on the variation you choose to  

60
00:06:48,200 --> 00:06:54,320
purchase. But we can see that the latest model from 
last year has 51 kilobytes which is a significant  

61
00:06:54,320 --> 00:07:03,080
increase. And on the other hand we had all along 
the STMicroelectronics ST33 which is also  

62
00:07:03,080 --> 00:07:09,240
a fairly mature product let's say. It has been 
around for quite some time now I mean definitely  

63
00:07:09,240 --> 00:07:14,680
more than three four years significantly 
more. And it has over 100 kilobytes and this  

64
00:07:14,680 --> 00:07:22,080
is plenty of storage for private key material, or 
for certificates, for user data, configuration data,  

65
00:07:22,080 --> 00:07:28,760
again we're not talking megabytes, so it is you 
advised to try to store a whole file there.  

66
00:07:28,760 --> 00:07:35,080
But at the same time you can store the symmetric 
cryptography key. Although there are better ways to do  

67
00:07:35,080 --> 00:07:41,120
it of course you can encrypt it using the TPM. 
I'm just trying to suggest here that having  

68
00:07:41,120 --> 00:07:48,040
that storage gives you a lot of possibilities how 
to increase the security of the system. Including  

69
00:07:48,040 --> 00:07:54,760
the TPM's NVRAM has some special properties that 
we will look in a later lecture. I'll just mention  

70
00:07:54,760 --> 00:08:00,840
that we can use the NVRAM as a secure counter, 
we can use it as an additional platform  

71
00:08:00,840 --> 00:08:05,240
configuration register, something that we'll 
discuss in the advanced TPM course. So this memory  

72
00:08:05,240 --> 00:08:12,880
size is limited but it is plenty to provide better 
security. At the same time be advised that using it  

73
00:08:12,880 --> 00:08:21,680
as a common secure storage is not a good approach. 
The last situation when a TPM is not suitable for  

74
00:08:21,680 --> 00:08:28,120
use is when we need a standalone security solution. 
And this is again by design, this is not some kind  

75
00:08:28,120 --> 00:08:36,160
of a drawback, but rather it is the purpose of the TPM; 
to be a passive device. We need a host device that  

76
00:08:36,160 --> 00:08:43,080
sends commands to the TPM to perform different 
operations. What this gives us is a high security  

77
00:08:43,080 --> 00:08:49,280
root of trust, and once ownership is established 
between the host and the TPM, then we can leverage  

78
00:08:49,280 --> 00:08:54,160
these roots of trust for storage, for reporting, and 
so on. Now there are different flavors of a TPM of  

79
00:08:54,160 --> 00:08:59,960
course but all of these are passive. We can look at 
some alternative solutions like for example  

80
00:08:59,960 --> 00:09:06,640
OpenTitan which has a TPM mode. At the same time 
OpenTitan can be an active device. An active  

81
00:09:06,640 --> 00:09:12,840
device means that it can operate by itself, it 
can have its own logic perform for example it can  

82
00:09:12,840 --> 00:09:18,680
perform a hash and make a decision based on this 
hash. While with the TPM we would send a command  

83
00:09:18,680 --> 00:09:26,800
from our main software or main CPU or secure 
isolated trusted execution environment. And you  

84
00:09:26,800 --> 00:09:34,440
would say to the TPM "Hey, please compute hash 
over this data" or "sign digitally this message". And  

85
00:09:34,440 --> 00:09:40,680
then we would send over the digest, the signature, over 
some communication channel or make the decision  

86
00:09:40,680 --> 00:09:46,160
in the secure software or on the main CPU and so 
on and so on. So there is a important difference  

87
00:09:46,160 --> 00:09:52,920
here and when first seeing the TPM as a security 
solution you might think that it is something that  

88
00:09:52,920 --> 00:09:57,920
can be used stand alone but this is not the 
case. This is by design. And it is important to  

89
00:09:57,920 --> 00:10:03,120
remember this especially if this is your 
first experience with a TPM. That being said  

90
00:10:03,120 --> 00:10:09,960
we don't know what the future holds fully, there 
could be one day TPM 3.0, and it could have some  

91
00:10:09,960 --> 00:10:15,880
kind of a mode switch to move from passive to 
active. But this would be a speculation at this  

92
00:10:15,880 --> 00:10:25,160
point. For now if you purchase a TPM it is 100% 
in all cases passive device, doesn't matter if it's  

93
00:10:25,160 --> 00:10:32,400
a UEFI module, firmware version, or discrete chip 
solution. In summary if you don't need significant  

94
00:10:32,400 --> 00:10:37,880
cryptographic acceleration, but you do need high 
guarantees for the security of your product of  

95
00:10:37,880 --> 00:10:44,680
your system, then the TPM is a great fit. If you 
don't need a significant amount of secure storage  

96
00:10:44,680 --> 00:10:51,720
but you are okay with having encryption as the 
system starts or on demand, and the keys for that  

97
00:10:51,720 --> 00:10:58,400
encryption can be protected by the TPM, then again 
the TPM is a really good fit. If you need a very  

98
00:10:58,400 --> 00:11:05,960
fast access to your disk, and encrypt and decrypt 
or your storage on the fly, then maybe it's best  

99
00:11:05,960 --> 00:11:13,000
to look for in an integrated solution. Either 
some kind of an encrypted SSD that has actually a  

100
00:11:13,000 --> 00:11:18,920
TPM built in and does all these operations for you, 
or some other types of solution. But typically TPM  

101
00:11:18,920 --> 00:11:25,800
is very good at providing you key infrastructure 
to protect your encryption keys and then unlocking  

102
00:11:25,800 --> 00:11:31,880
bits of the system or the whole system. This is 
how my BitLocker works. So for secure storage  

103
00:11:31,880 --> 00:11:39,440
the TPM has its purposes just do not intend to use 
the non-volatile memory of the TPM as the one and  

104
00:11:39,440 --> 00:11:47,000
only secure storage. Be prepared to have artifacts 
and data outside the TPM that this protected, is  

105
00:11:47,000 --> 00:11:53,680
encrypted, by the TPM. That being said the  

106
00:11:53,680 --> 00:11:59,000
last piece. If you're looking for an active device 
for an active solution where it can just work by  

107
00:11:59,000 --> 00:12:05,160
by itself, you are intending to write your own 
program or logic and you just want to put it in a  

108
00:12:05,160 --> 00:12:10,720
TPM unfortunately that's not possible. There are 
some customization options regarding algorithms  

109
00:12:10,720 --> 00:12:16,480
but having your own logic inside the TPM this is 
not something possible at this moment with TPM 2.0.  

110
00:12:16,480 --> 00:12:23,960
And it is intended, so the TPM is a passive 
device need to have a host CPU MCU something to  

111
00:12:23,960 --> 00:12:30,200
issue commands to the TPM, take ownership of the 
TPM hierarchies, and operate it. The logic lives with  

112
00:12:30,200 --> 00:12:38,680
the secure trusted execution environment, memory 
isolated process, or some other form of host.  

113
00:12:38,680 --> 00:12:45,480
Now that we've covered when not to use TPM, and all 
the other cases are very likely a good fit, I would  

114
00:12:45,480 --> 00:12:52,160
like to go over again why the TPM has advantages 
over other solutions like a secure element or  

115
00:12:52,160 --> 00:12:57,960
maybe some other forms of HSM like smart 
cards which are quite common even nowadays. I  

116
00:12:57,960 --> 00:13:03,200
really like the fact that the TPM has a built-in 
protection against Machine-in-the-Middle attacks.  

117
00:13:03,200 --> 00:13:08,480
Of course this protection needs to be enabled 
and we have a whole lecture and exercises on this  

118
00:13:08,480 --> 00:13:15,680
topic. It is often that in off the shelf devices 
OEMs and OS vendors do not enable this protection  

119
00:13:15,680 --> 00:13:21,840
early on or forget to enable it all together. But 
this is not a fault of the TPM architecture or  

120
00:13:21,840 --> 00:13:27,240
design, this is not a fault of the TPM vendors. It 
is something that we need to be aware that there  

121
00:13:27,240 --> 00:13:32,000
is a protection with we just need to enable it. 
Another big benefit of the TPM is that once you  

122
00:13:32,000 --> 00:13:38,640
create your solution the TPM uses a standardized 
command set. The Trusted Computing Group has built a  

123
00:13:38,640 --> 00:13:45,560
specification, it's publicly available. And you have 
the guarantee that if you change your TPM vendor  

124
00:13:45,560 --> 00:13:50,400
your application your solution would work the 
same way. Of course there could be variances in the  

125
00:13:50,400 --> 00:13:57,440
algorithm or maybe a bit in the speed as we saw  on the benchmarking slide. But the operations  

126
00:13:57,440 --> 00:14:02,840
should work exactly the same way and you would get 
the same result and the same security guarantees.  

127
00:14:02,840 --> 00:14:09,040
One major difference that often becomes overlooked 
is that the physical tamper protection of a TPM,  

128
00:14:09,040 --> 00:14:15,400
and we would need to look closer at the data 
sheets, but usually it's significantly higher level  

129
00:14:15,400 --> 00:14:22,440
than what secure elements offer. And comparing the 
prices I think it's better to opt for a TPM when  

130
00:14:22,440 --> 00:14:30,560
higher security needs exist. I already mentioned 
there are several TPM vendors in the world. I wanted to  

131
00:14:30,560 --> 00:14:36,000
visualize that they are placed in different 
places of the world and of course there's a  

132
00:14:36,000 --> 00:14:43,680
worldwide supply chain. Some of these TPM vendors 
have plants factories in to fabricate the  

133
00:14:43,680 --> 00:14:50,360
silicon chips in different places, US, Germany, and 
so on. And this gives you flexibility that gives  

134
00:14:50,360 --> 00:14:58,760
you some kind of a redundancy. Which I think is 
important in today's world of creating Internet  

135
00:14:58,760 --> 00:15:05,280
of Things devices where we need to have parts to 
substitute in case something happens. And the TPM  

136
00:15:05,280 --> 00:15:11,240
offers this which is a great advantage compared to 
if you're using a secure element and you decide  

137
00:15:11,240 --> 00:15:16,280
to switch from one vendor to the other it would be 
a completely different commands set. It might even  

138
00:15:16,280 --> 00:15:22,520
be a different bus. So in this case this really 
lowers your cost and it allows again your solution  

139
00:15:22,520 --> 00:15:27,920
to just work. What you had let's say with an Infineon 
TPM will work the same way with an ST TPM and what  

140
00:15:27,920 --> 00:15:34,840
you had with a Nuvoton TPM will work the same way with 
an Infineon TPM. There are very very rare cases where  

141
00:15:34,840 --> 00:15:40,320
this will not be true. And having this mature 
market also guarantees the supply chain. Which  

142
00:15:40,320 --> 00:15:47,280
is very important in recent days. And the last 
bit from the developers perspective, from the time  

143
00:15:47,280 --> 00:15:54,000
to market perspective, again is that the software 
stack that provides us the interface to the TPM  

144
00:15:54,000 --> 00:16:00,920
is also very flexible. We have variety of libraries 
as we already saw and they cover everything from  

145
00:16:00,920 --> 00:16:07,920
server-cloud situation to Internet of Things bare 
metal application. Depending on what you need you  

146
00:16:07,920 --> 00:16:13,960
can just choose the software stack you like, get TPM 
from the vendor that you prefer, closest to you, or  

147
00:16:13,960 --> 00:16:22,040
fastest, or maybe has the largest non-volatile 
memory, and start working. And in a later version  

148
00:16:22,040 --> 00:16:28,080
you need TPM with less memory you can just switch TPM and it just works. I think this is a major  

149
00:16:28,080 --> 00:16:34,000
major benefit when comparing TPM with any other 
HSM solutions out there. Here are the software  

150
00:16:34,000 --> 00:16:41,960
libraries. And just quickly going through this we have the tpm2 TSS which follows the TCG  

151
00:16:41,960 --> 00:16:47,840
specification this is the only stack that conforms 
with the Trusted Computing Group specification.  

152
00:16:47,840 --> 00:16:55,600
It offers three different types of API. The IBM 
TSS, which comes in C programming language.  

153
00:16:55,600 --> 00:17:03,280
It comes also with its own set of tools. Then we 
have the go TPM originally created by Google.  

154
00:17:03,280 --> 00:17:08,800
Now contributed... now a lot of people are contributing 
and companies of course best for cloud and servers. 

155
00:17:08,800 --> 00:17:14,040
wolfTPM designed for bare metal. Can be used in 
rich systems as well as you can see on the right  

156
00:17:14,040 --> 00:17:19,600
side of the table, all of these stacks can be used 
under windows and Linux. And when I say Linux I  

157
00:17:19,600 --> 00:17:25,200
need probably to mention also this includes Mac 
OS. In some situations it you might need to tweak  

158
00:17:25,200 --> 00:17:31,400
a bit the build command at first. But this is 
fairly well documented. For example I remember very  

159
00:17:31,400 --> 00:17:37,120
well the IBM stack has a documentation where 
there's specific paragraph that just says use  

160
00:17:37,120 --> 00:17:43,960
this makefile for Mac and it works. So the last 
one not to forget the Microsoft build TPM stack.  

161
00:17:43,960 --> 00:17:48,360
I would definitely recommend this stack if you're 
in a Windows environment. I had such situation with  

162
00:17:48,360 --> 00:17:55,680
a customer car manufacturer and this was the 
natural choice. So this flexibility is what I  

163
00:17:55,680 --> 00:18:01,280
was talking about. Depending on your situation 
you can choose the best library for you.  

164
00:18:01,280 --> 00:18:07,320
This is not the case with secure elements typically 
you have one or two options maybe one from the  

165
00:18:07,320 --> 00:18:15,600
vendor and one an external project. And this 
gives you the flexibility to develop quicker, and  

166
00:18:15,600 --> 00:18:22,680
migrate between platform as your products mature, 
and grows and has next versions and so on.  

167
00:18:22,680 --> 00:18:31,120
We've already covered good use cases for the TPM. I just 
want to reiterate some real life scenarios, where  

168
00:18:31,120 --> 00:18:40,240
we've seen an increasing use of x509 certificates, 
and the TPM can be used to back such operations  

169
00:18:40,240 --> 00:18:45,200
as the certificate signing requests. Which you 
know are vital during provisioning, during right  

170
00:18:45,200 --> 00:18:51,880
revocation, and various operations in the life 
cycle of a device. Of any system really. On the  

171
00:18:51,880 --> 00:18:58,920
other hand we have the TPM sealing capabilities 
either of arbitrary data, or whole files, or we  

172
00:18:58,920 --> 00:19:05,800
can seal against the state of the system and then 
only unlock the encrypting key for a hard drive.  

173
00:19:05,800 --> 00:19:12,480
Meaning that we can actually lock down the system 
against an authorized use, or against system that  

174
00:19:12,480 --> 00:19:21,120
is different from what it was, from its good known 
state. This is very often a TPM very very common  

175
00:19:21,120 --> 00:19:27,960
TPM application to use case. And of course I 
really want to highlight the possibility to use  

176
00:19:27,960 --> 00:19:34,520
a TPM-backed keys to establish TLS connection. 
Which further increases the security of this  

177
00:19:34,520 --> 00:19:41,920
protection against Machine-in-the-Middle over the 
Internet. The TPM secure storage has plenty of room  

178
00:19:41,920 --> 00:19:50,120
usually to store several certificates to store 
multiple multiple NV counters non-volatile counters  

179
00:19:50,120 --> 00:19:55,560
which we save between power cycles. And only with 
property TPM authorization we can use that. There's  

180
00:19:55,560 --> 00:20:03,240
a use case with taxi drivers actually where 
the taxis are supplied with a device that uses  

181
00:20:03,240 --> 00:20:09,840
a TPM to count the mileage that has been covered. 
There are other use cases for trucks and whatnot.  

182
00:20:09,840 --> 00:20:16,240
But the counter itself could be just an indicator 
for various events that have occurred, and this  

183
00:20:16,240 --> 00:20:22,600
could be tracking temperature of medical devices 
while shipping, or something else really, it could  

184
00:20:22,600 --> 00:20:29,080
be opening a door when the door should have been 
closed. Then the TPMs NVRAM might not be large  

185
00:20:29,080 --> 00:20:34,800
in size, and I keep repeating this because it is 
important to remember, but we can use it to store  

186
00:20:34,800 --> 00:20:44,080
sensitive data. Now I've seen examples in my career 
where it's tempting to just store a symmetric key  

187
00:20:44,080 --> 00:20:50,600
there. And that's okay depending how it is designed. 
But better is to use the TPM keys to encrypt this  

188
00:20:50,600 --> 00:20:55,520
symmetric key, but that's also one way to go about 
this. Sometimes I've seen certificates that are  

189
00:20:55,520 --> 00:21:02,920
needed to authenticate, I've seen some kind of of a 
license structure also being kept at the TPMs NVRAM  

190
00:21:02,920 --> 00:21:09,320
and not being unlocked unless given states are 
made or given authorization. So there are many many  

191
00:21:09,320 --> 00:21:14,840
applications for the TPM secure storage. I think 
this is something special about the TPM compared  

192
00:21:14,840 --> 00:21:20,240
to other HSM that it has a separate root of trust 
for reporting. This comes from the endorsement  

193
00:21:20,240 --> 00:21:26,040
hierarchy. Which provides us unique machine identity, 
it can be used as a unique machine identity, and  

194
00:21:26,040 --> 00:21:32,800
from there we can generate different cryptographic 
proofs. Would that be just a digital signature,  

195
00:21:32,800 --> 00:21:38,360
would it be something more like a hash and then a 
signature, or directly the tpm2_quote that we will  

196
00:21:38,360 --> 00:21:44,560
look in the advanced TPM class. I think this is a 
major advantage of the TPM security capabilities.  

197
00:21:44,560 --> 00:21:49,800
To compare the system state to its golden state, 
and maybe we verify just the configuration of  

198
00:21:49,800 --> 00:21:56,720
the system, or maybe we verify a whole partition 
that holds let's say the application or a folder.  

199
00:21:56,720 --> 00:22:03,200
There are many many use cases of this feature. 
Now another interesting aspect of the TPMs root  

200
00:22:03,200 --> 00:22:11,440
of trust is that it has a command and a way to 
certify that keys originated from a specific TPM.  

201
00:22:11,440 --> 00:22:17,240
Or that that this TPM key is right now loaded 
into the system. This is very important for  

202
00:22:17,240 --> 00:22:23,960
some sensitive operations like firmware upgrade or 
again transferring important parameters to your  

203
00:22:23,960 --> 00:22:29,520
system to your device. It could be licensing this 
is something I see people come as a first  

204
00:22:29,520 --> 00:22:35,400
thought often. There are way more use cases than 
licensing when we need to think about the TPM.  

205
00:22:35,400 --> 00:22:41,440
Again, parameters for industrial machines this is 
critical information. Licensing is just a natural  

206
00:22:41,440 --> 00:22:47,000
thing for this but it's definitely much more 
valuable to secure and protect the configuration  

207
00:22:47,000 --> 00:22:52,120
parameters that we enter industrial equipment.
To avoid incidents, to avoid malicious attacks.  

208
00:22:52,120 --> 00:22:59,360
So I think the proofs that we can generate using 
the TPM, if a certain key is present, if a certain  

209
00:22:59,360 --> 00:23:04,040
operation took place, if a certain state of the 
system is true at this moment, not just at the  

210
00:23:04,040 --> 00:23:10,160
start of the system but right now. This gives us 
a higher security level and we can then safely  

211
00:23:10,160 --> 00:23:15,720
proceed with let's say new parameterization of 
the industrial equipment or of the automated  

212
00:23:15,720 --> 00:23:22,960
crane. Including we had a situation at TPM.dev once where I think it was the third largest  

213
00:23:22,960 --> 00:23:30,840
automated car wash company was looking to 
integrate TPM into their systems. In summary we  

214
00:23:30,840 --> 00:23:38,200
need to remember that TPMs are passive devices. 
They need a host they need a program preferably  

215
00:23:38,200 --> 00:23:43,600
living in a secure environment or memory isolated 
environment that sends commands to the TPM. TPM  

216
00:23:43,600 --> 00:23:49,680
cannot act on its own. And there are standards by 
the TCG there is functional testing to provide  

217
00:23:49,680 --> 00:23:54,840
evidence that how the TPM operates so we can trust 
it. There is a mechanism to verify that the  

218
00:23:54,840 --> 00:24:01,720
TPM is genuine. And we'll talk about this a bit 
later. The TPM has non-volatile memory that is  

219
00:24:01,720 --> 00:24:07,520
very useful because of the TPM authorization, and 
the protection the TPM has as a physical chip.  

220
00:24:07,520 --> 00:24:14,760
At the same time we need to select carefully what we 
store there because of its limited size. We can  

221
00:24:14,760 --> 00:24:22,000
get over 100 kilobytes which is a lot for sensitive 
data. In most situations this is more than enough  

222
00:24:22,000 --> 00:24:28,960
and we can use the TPM as a bridge as just a 
step to decrypting our sensitive file storage , 

223
00:24:28,960 --> 00:24:34,520
or partition, or external drive, and so on. If you 
need something more specific you can always get  

224
00:24:34,520 --> 00:24:41,000
an integrated solution. Usually if you take an 
SSD that has encryption decryption built in  

225
00:24:41,000 --> 00:24:46,000
it comes actually already with the TPM. So the 
TPM is really good solution for secure storage.  

226
00:24:46,000 --> 00:24:55,080
Just be mindful of its NVRAM size. And last but 
not least compared to other HSM solutions, TPM is  

227
00:24:55,080 --> 00:25:02,120
very affordable and provides higher physical tamper 
protection, higher side channel attacks protection.  

228
00:25:02,120 --> 00:25:08,720
And I think it's worth looking into the TPM when 
comparing with secure elements and other similar

229
00:25:08,720 --> 00:25:14,400
solutions.