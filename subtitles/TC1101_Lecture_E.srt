1
00:00:00,880 --> 00:00:07,760
Welcome to this OST2 lecture about
performing HMAC and hashing using a TPM.

2
00:00:07,760 --> 00:00:14,679
Randomness is important to
cryptography because computers are

3
00:00:14,679 --> 00:00:20,160
really good at producing the same output
when we provide them the same input. 

4
00:00:20,160 --> 00:00:27,199
And for cryptography it is important to have
guaranteed difference in our keys, in the

5
00:00:27,199 --> 00:00:31,679
output of the cryptographic operations.
For this we have some source of

6
00:00:31,679 --> 00:00:37,559
randomness, and a cryptographic algorithm,
that is usually an implementation of

7
00:00:37,559 --> 00:00:43,239
different mathematical
operations. We have great examples what

8
00:00:43,239 --> 00:00:50,960
happens when our source of randomness
fails. Or, if we fail to use the source of

9
00:00:50,960 --> 00:00:59,160
randomness to generate new fresh values
for our operations. For example, we have

10
00:00:59,160 --> 00:01:03,040
the situation
with the Debian and Ubuntu flavors of

11
00:01:03,040 --> 00:01:10,799
Linux where a small change in the
OpenSSL package for Debian and Ubuntu, that

12
00:01:10,799 --> 00:01:18,360
was on a redundancy code, impacted the
randomness of the number generator.

13
00:01:18,360 --> 00:01:26,479
By lowering the entropy, and how different
the numbers are, this provided attack

14
00:01:26,479 --> 00:01:33,159
surface for different attacks. On SSH keys,
VPN keys, DNSSEC and so on.

15
00:01:33,159 --> 00:01:39,079
The TPM seed for our endorsement
hierarchy, endorsement keys, and other

16
00:01:39,079 --> 00:01:45,799
primary keys can also be used for random
number generation. There is a tpm2-tool,

17
00:01:45,799 --> 00:01:53,320
with the nice name tpm2_getrandom, that
can provide almost unlimited amount of

18
00:01:53,320 --> 00:02:01,920
random numbers, of course, there is a catch. At once
the TPM can provide only as much as the largest

19
00:02:01,920 --> 00:02:10,840
supported hash digest. Back in the day this was
32 bytes because of SHA-256, nowadays we have TPMs

20
00:02:10,840 --> 00:02:19,320
that support up to SHA-512 so we get 64 bytes at
once. And we can make multiple calls to the TPM

21
00:02:19,320 --> 00:02:25,280
random number generator and get as many bytes
as we need. The tool has a fairly standard set

22
00:02:25,280 --> 00:02:32,760
of options. By using -o we can have the output
stored in a file instead of going to the standard

23
00:02:32,760 --> 00:02:39,560
output. By using --hex we can have
it in a hex format. And something special about

24
00:02:39,560 --> 00:02:48,080
this tool is the presence of a -f for forcing
to override safety checks. Essentially, you would

25
00:02:48,080 --> 00:02:55,640
not received any random bytes if they're not the
exact amount requested. This is very rare it would

26
00:02:55,640 --> 00:03:03,280
show some kind of a malfunction, or you've tried
to exceed the supported hash digest size. Let's

27
00:03:03,280 --> 00:03:11,320
say you want to generate 80 bytes and the TPM can
provide only 64 at once. We have the definition on

28
00:03:11,320 --> 00:03:18,520
the slide I'll just read it out. One-way computation
of any size of data to a fixed size result. This is

29
00:03:18,520 --> 00:03:24,840
particularly useful for security purposes when
we want to obfuscate a secret or a text usually a

30
00:03:24,840 --> 00:03:33,360
password. This is also available on Linux system
using the shadow file in the /etc folder. Because

31
00:03:33,360 --> 00:03:41,480
the hashing is so useful you probably already
have heard about algorithms such as MD5, SHA-1.

32
00:03:41,480 --> 00:03:48,680
Unfortunately, both of these algorithms are not
good for security purposes nowadays. And we often

33
00:03:48,680 --> 00:03:55,960
use SHA-256. And the recommendation is to use
the newer versions. The good thing is that the

34
00:03:55,960 --> 00:04:04,160
TPM supports these hash versions. And you would
still need to check your specific TPM model and

35
00:04:04,160 --> 00:04:12,880
TPM firmware. Because TPMs have a minimum set of
functionalities like the SHA-256. But everything

36
00:04:12,880 --> 00:04:18,840
else is more or less optional. And although
the industry has moved in that direction, and

37
00:04:18,840 --> 00:04:26,320
now it's a the facto standard to have SHA-512,
depending when your machine was built or when

38
00:04:26,320 --> 00:04:32,880
the server was built it is always best to check
what is really actually supported by the TPM that

39
00:04:32,880 --> 00:04:41,585
you're currently using. To perform hashing using a
TPM we have a another TPM tool with nice name tpm2_hash.

40
00:04:41,585 --> 00:04:48,840
Similar to other tools as the last argument
it takes a file name. Otherwise it would read the

41
00:04:48,840 --> 00:04:55,160
data straight from the standard input. Here we are
required to specify hashing algorithm using the

42
00:04:55,160 --> 00:05:02,160
-g option. We also use this option when generating
keys. And you can check the supported algorithms, by

43
00:05:02,160 --> 00:05:07,960
the tpm2-tools, how the different abbreviations
and names translate. Usually it's a one to one

44
00:05:07,960 --> 00:05:15,535
mapping. And we have our standard --hex and -o
options that we are very familiar by now.

45
00:05:15,535 --> 00:05:22,520
Last but definitely not least is the HMAC operation.
It provides us the benefit of verifying the data

46
00:05:22,520 --> 00:05:28,440
integrity and the origin of the message at
the same time. And we can just use a shared

47
00:05:28,440 --> 00:05:35,320
secret between the two hosts or the entities that
need to communicate securely. One specific of the

48
00:05:35,320 --> 00:05:43,440
HMAC is that by design the message used in the
operation is not encrypted. So we would need to

49
00:05:43,440 --> 00:05:51,360
have a means to do that. To guarantee also no one
else can intercept our message. To perform HMAC

50
00:05:51,360 --> 00:05:59,560
we need a TPM key of key hash type, otherwise the
tpm2_hmac tool would complain and the HMAC operation

51
00:05:59,560 --> 00:06:06,360
will not complete. The only mandatory argument
here is the actual input data. This could come

52
00:06:06,360 --> 00:06:13,200
from a file or the standard input can be used.
The hashing algorithm by default is SHA-256 and this

53
00:06:13,200 --> 00:06:20,280
is good enough. You can always opt in for
a stronger hash algorithm which would result in

54
00:06:20,280 --> 00:06:27,480
a stronger HMAC. We have OTP generation in our
exercises. And while this is good for training

55
00:06:27,480 --> 00:06:33,920
I wanted to present you how it would look in
practice. In reality we need a shared key.

56
00:06:33,920 --> 00:06:41,320
But by design TPM Keys cannot be shared with other
entities. Of course there is a way to have a key

57
00:06:41,320 --> 00:06:47,520
that can be shared between TPMs. This is why we had
the fixed TPM attribute that can be set or unset

58
00:06:47,520 --> 00:06:53,680
There's also a possibility to duplicate keys.
But if you want to share a key with another host or

59
00:06:53,680 --> 00:07:02,280
another entity, device does not have a TPM, then you
need the actual key in plain (text). Okay TPM cannot just

60
00:07:02,280 --> 00:07:09,320
use that. So we need to import that key inside the
TPM. This operation is explained in our advanced

61
00:07:09,320 --> 00:07:17,160
TPM course. For completeness here I have outlined
the flow of practical generation of of a one-time

62
00:07:17,160 --> 00:07:22,840
password using TPM. What I would recommend is
to generate random data for a symmetric key.

63
00:07:22,840 --> 00:07:31,120
And then import this key into the TPM on the
other host securely share the random data.

64
00:07:31,120 --> 00:07:37,680
At this stage, the secret is exchanged and what we need
is just a counter. The counter can be a file can

65
00:07:37,680 --> 00:07:43,800
be an actual digit with ASCII representation.
It's up to you in the implementation. Once we

66
00:07:43,800 --> 00:07:49,440
have imported the TPM key and share securely the
symmetric key data, we just need to remember to

67
00:07:49,440 --> 00:07:55,520
increment the counter every time we need to make a
challenge and a verification. As long as a counter

68
00:07:55,520 --> 00:08:02,720
matches on both sides of the host the generated
HMAC should be the same. An typical implementations

69
00:08:02,720 --> 00:08:08,960
because of a user error or system error the
counter could get ahead. What we see is that

70
00:08:08,960 --> 00:08:17,240
the two hosts perform multiple HMAC computations
with the last known counter value plus minus 5

71
00:08:17,240 --> 00:08:23,600
ahead. So let's say we have a counter of 5
the HMAC on the server side would try the HMAC

72
00:08:23,600 --> 00:08:30,960
computation with the counter being 6, 7, 8, 9, and 10.
Once a matching value is found with the other host,

73
00:08:30,960 --> 00:08:37,640
this counter will be taken as the current one.
So it could be that either attempts failed or

74
00:08:37,640 --> 00:08:44,000
one of the host for various reasons went ahead.
But the previous values of the counter are no

75
00:08:44,000 --> 00:08:49,720
longer valid and the counter would only move up.
I hope this example of practical one-time password

76
00:08:49,720 --> 00:08:56,120
generation was useful. Please bear in mind that
this is not time-based one-time password generation.

77
00:08:56,120 --> 00:09:04,480
This is HMAC based or briefly said HOTP. If you
need help feel free to write us questions in the

78
00:09:04,480 --> 00:09:10,400
discussion boards after each unit this helps us
understand the context of your question and where

79
00:09:10,400 --> 00:09:16,960
the problem originated also we have an email for
any questions and any inquiries feel free to reach

80
00:09:16,960 --> 00:09:20,000
out.